<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_starterkit_hotel\Functional;

use Drupal\Tests\schemadotorg\Functional\SchemaDotOrgBrowserTestBase;

/**
 * Tests the functionality of the Schema.org Starter Kit: Hotel module.
 *
 * @group schemadotorg_starterkit_hotel
 */
class SchemaDotOrgStarterKitHotelTest extends SchemaDotOrgBrowserTestBase {

  // phpcs:disable
  /**
   * Disabled config schema checking.
   */
  protected $strictConfigSchema = FALSE;
  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['schemadotorg_starterkit_hotel'];

  /**
   * Test Schema.org Starter Kit: Hotel.
   */
  public function testHotel(): void {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    // Check that amenities as imported via hook_install().
    $terms = $term_storage->loadByProperties(['name' => 'AC']);
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = reset($terms);
    $this->assertEquals('AC', $term->label());
    $terms = $term_storage->loadByProperties(['name' => 'Airport shuttle']);
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = reset($terms);
    $this->assertEquals('Airport shuttle', $term->label());
  }

}
