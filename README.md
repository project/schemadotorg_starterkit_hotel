Table of contents
-----------------

* Introduction
* Features
* Notes
* References


Introduction
------------

The **Schema.org Blueprints Starter Kit: Hotel module** provides
Schema.org types required to build a hotel website.


Features
--------

- Provides Person (person), Organization (organization), Hotel (hotel)
  and HotelRoom (hotel_room) content types.
- Adds an Amenities vocabulary.
- Creates a /hotels view.
- Adds a default shortcut to view Hotels (/hotels).


Notes
-----

### [Markup for Hotels](https://schema.org/docs/hotels.html)

- Primary Schema.org types
  - https://schema.org/LodgingBusiness
  - https://schema.org/Accommodation
  - https://schema.org/BedDetails
  - https://schema.org/Offer
- Booking Schema.org types
  - https://schema.org/Product
  - https://schema.org/LodgingReservation

- A https://schema.org/HotelRoom can also be identified as a https://schema.org/Product.
- The https://schema.org/additionalType property allows indicating the exact type.
- [Pricing](https://schema.org/docs/hotels.html#pricing) should be a sub-module.

### [Hotel Price Structured Data Reference](https://developers.google.com/hotels/hotel-prices/structured-data/hotel-price-structured-data)

- Primarily focused on understanding what is offered with pricing at the hotel.
- https://schema.org/amenityFeature is important.
- https://schema.org/identifier is recommended.
- https://schema.org/makesOffer and https://schema.org/priceSpecification is used for commerce.

### [Google Lodging Format Schema | Hotel Content](https://developers.google.com/hotels/hotel-content/proto-reference/lodging-schema)

- Defines expected values for common amenities and services.

### Examples

Below are examples of hotels considered while building this starter kit.

- [The Porches Inn](https://porches.com/)
- [TWA Hotel](https://www.twahotel.com/)
- [Hotel Chelsea](https://hotelchelsea.com/)
- [Ace Hotel New York](https://acehotel.com/new-york/)


### Types

- **Amenities** (taxonomy_term:LocationFeatureSpecification)  
  <https://schema.org/LocationFeatureSpecification>

- **Bed Details** (paragraph:BedDetails)  
  <https://schema.org/BedDetails>

- **Contact Point** (paragraph:ContactPoint)  
  <https://schema.org/ContactPoint>

- **Person** (node:Person)  
  A person (alive, dead, undead, or fictional).  
  <https://schema.org/Person>

- **Organization** (node:Organization)  
  An organization such as a school, NGO, corporation, club, etc.  
  <https://schema.org/Organization>

- **Hotel Room** (node:HotelRoom)  
  A hotel room is a single room in a hotel.  
  <https://schema.org/HotelRoom>

- **Hotel** (node:Hotel)  
  A hotel is an establishment that provides lodging paid on a short-term basis.  
  <https://schema.org/Hotel>


References
----------

- [Data Model for a Hotel Management System](https://vertabelo.com/blog/data-model-for-hotel-management-system/)
